/**
 * Members: aluc, dluc, mwang6
 * client.cpp
 */

#include <ps_client/client.h>

/* CALLBACKS ----------------------------------------------------------------------------------*/

Callback::Callback() {}

void EchoCallback::run(Message &m) {
	log("Dispatcher: Received message %s from %s (%zu bytes)", m.topic_.c_str(), m.sender_.c_str(), m.body_.length());
}

void AppCallback::run(Message &m) {
	std::time_t result = std::time(nullptr);
	// std::cout << "[" << m.topic_ << " - " << std::asctime(std::localtime(&result)) << "]";
	// std::cout << "[" << m.sender_ << "]:" << m.body_ << std::endl;
	log("[%s - %s] [%s]: %s", m.topic_.c_str(), std::asctime(std::localtime(&result)), m.sender_.c_str(), m.body_.c_str());
}

/* CLIENT -----------------------------------------------------------------------------------*/

Client::Client(std::string host, std::string port, std::string cid) : outgoing_() {
	/* Store client default values */
	host_ = host;
	port_ = port;
	cid_ = cid;
	nonce_ = 123;

	/* Create socket */
	server_file_ = make_socket(host_, port_);
	connect_server();
}

void Client::connect_server() {
	Message m;
	m.type_ = "IDENTIFY";
	m.topic_ = "";
	m.sender_ = cid_;
	m.nonce_ = nonce_;
	m.body_ = "";

	outgoing_.push(m);
}

void Client::publish(std::string topic, std::string message, size_t length) {
	Message m;
	m.type_ = "PUBLISH";
	m.topic_ = topic;
	m.sender_ = cid_;
	m.nonce_ = nonce_;
	m.body_ = message;

	outgoing_.push(m);
}

void Client::subscribe(std::string topic, Callback *callback) {
	Message m;
	m.type_ = "SUBSCRIBE";
	m.topic_ = topic;
	m.sender_ = cid_;
	m.nonce_ = nonce_;
	m.body_ = "";

	outgoing_.push(m);
	
	// Put Callback into map.
	if (callback_map.find(topic) == callback_map.end()) {
		callback_map.insert(std::make_pair(topic, callback));		
	}
}

void Client::unsubscribe(std::string topic) {
	Message m;
	m.type_ = "UNSUBSCRIBE";
	m.topic_ = topic;
	m.sender_ = cid_;
	m.nonce_ = nonce_;
	m.body_ = "";

	outgoing_.push(m);
}

void Client::disconnect() {
	Message m;
	m.type_ = "DISCONNECT";
	m.topic_ = "";
	m.sender_ = cid_;
	m.nonce_ = nonce_;
	m.body_ = "";

	int rc;
	Pthread_mutex_lock(&lock_);
	is_shutdown_ = true;
	Pthread_mutex_unlock(&lock_);

	outgoing_.push(m);
}

void Client::run() {
	// Declare threads with Client class.
	Thread send((void *)this);
	Thread receive((void *)this);
	Thread process((void *)this);

	// Initialize results.
	size_t result1;
	size_t result2;
	size_t result3;

	// Start threads.
	send.start(send_thread, (void *)this);
	receive.start(receive_thread, (void *)this);
	process.start(process_thread, (void *)this);

	// Join threads.
	send.join((void **)&result1);
	receive.join((void **)&result2);
	process.join((void **)&result3);

	// Check join results.
	if (result1 != 0 && result2 != 0 && result3 != 0) {
		std::cout << "ERROR Client Run threads unsuccessfully joined" << std::endl;
	}
}

bool Client::shutdown() {
	int rc;
	Pthread_mutex_lock(&lock_);
	bool flag = is_shutdown_;
	Pthread_mutex_unlock(&lock_);
	return flag;
}


/* CLIENT THREADS ----------------------------------------------------------------------------------*/

void *Client::send_thread(void *client) {
	Client *c = (Client *)client;
	char buffer[BUFSIZ];
	FILE *thread_socket_ = make_socket(c->host_, c->port_);

	while (!(c->shutdown())) {
		Message m = c->outgoing_.pop();

		// Send to server.
		if (m.type_.compare("IDENTIFY") == 0) {
			fprintf(thread_socket_, "%s %s %zu\n", m.type_.c_str(), m.sender_.c_str(), m.nonce_);
			fgets(buffer, BUFSIZ, thread_socket_);
			log("%s", buffer);
		}
		else if (m.type_.compare("PUBLISH") == 0) {
			fprintf(thread_socket_, "%s %s %zu\n%s", m.type_.c_str(), m.topic_.c_str(), m.body_.length(), m.body_.c_str());
			fgets(buffer, BUFSIZ, thread_socket_);
			log("Publisher: %s", buffer);
		}
		else if (m.type_.compare("SUBSCRIBE") == 0) {
			fprintf(thread_socket_, "%s %s\n", m.type_.c_str(), m.topic_.c_str());
			fgets(buffer, BUFSIZ, thread_socket_);
			log("Publisher: %s", buffer);
		}
		else if (m.type_.compare("UNSUBSCRIBE") == 0) {
			fprintf(thread_socket_, "%s %s\n", m.type_.c_str(), m.topic_.c_str());
			fgets(buffer, BUFSIZ, thread_socket_);
			log("Publisher: %s", buffer);
		}
		else if (m.type_.compare("DISCONNECT") == 0) {
			fprintf(thread_socket_, "%s %s %zu\n", m.type_.c_str(), m.sender_.c_str(), m.nonce_);
			fgets(buffer, BUFSIZ, thread_socket_);
			log("Publisher: %s", buffer);
		}
		else {
			std::cout << "Error in type: " << m.type_ << std::endl;
		}
	}

	log("Publisher: %s", "exiting");

	return 0;
}

void *Client::receive_thread(void *client) {
	log("Trying to retrieve line %s", "186");

	Client *c = (Client *)client;
	FILE *thread_socket_ = make_socket(c->host_, c->port_);
	char buffer[BUFSIZ];

	// Connect.
	fprintf(thread_socket_, "IDENTIFY %s %zu\n", c->cid_.c_str(), c->nonce_); // TODO determine how to get nonce
	fgets(buffer, BUFSIZ, thread_socket_); // Gets the IDENTIFIED message

	// Receive messages.
	while (!(c->shutdown())) {
		Message m;
		char header[BUFSIZ];
		char body[BUFSIZ];
		char topic[BUFSIZ];
		char sender[BUFSIZ];
		char length[BUFSIZ];


		fprintf(thread_socket_, "RETRIEVE %s\n", c->cid_.c_str());
		fgets(header, BUFSIZ, thread_socket_);
		log("load_stream header: %s", header);
		
		sscanf(header, "MESSAGE %s FROM %s LENGTH %s", topic, sender, length);

		if (fgets(body, atoi(length)+1, thread_socket_) < 0) {
			perror("error");
		}

		log("load_stream body: %s", body);

		// Update Message struct.
		m.type_ = "RETRIEVE";  // TODO CHECK
		m.topic_ = topic;
		m.sender_ = sender;
		m.nonce_ = c->nonce_;
		m.body_ = body;  

		// Log received message.
		log("Subscriber: Received message %s from %s (%zu bytes)", m.topic_.c_str(), m.sender_.c_str(), m.body_.length());

		// Print message.
		std::cout << "topic\t= " << topic << std::endl;
		std::cout << "sender\t= " << sender << std::endl;
		std::cout << "length\t= " << atoi(length) << std::endl;
		std::cout << "body\t= " << body << std::endl;

		// Push message onto incoming queue.
		c->incoming_.push(m);
	}

	log("Subscriber: %s", "exiting");

	return 0;
}
void *Client::process_thread(void *client) {
	Client *c = (Client *)client;
	FILE *thread_socket_ = make_socket(c->host_, c->port_);
	char buffer[BUFSIZ];

	// Connect.
	fprintf(thread_socket_, "IDENTIFY %s %zu\n", c->cid_.c_str(), c->nonce_); // TODO determine how to get nonce
	fgets(buffer, BUFSIZ, thread_socket_); // Gets the IDENTIFIED message

	while (!(c->shutdown())) {
		Message m;
		Callback *cb;
		m = c->incoming_.pop();
		if (m.nonce_) {
			// Run callback for message.
			cb = c->callback_map[m.topic_];
			cb->run(m);
		} else {
			std::cout << "ERROR No callbacks for " << m.sender_ << std::endl;
		}
	}

	log("Dispatcher: %s", "exiting");

	return 0;
}

/* GETS -------------------------------------------------------------------------------------*/

std::string Client::get_host() {
	return host_;
}

std::string Client::get_port() {
	return port_;
}

std::string Client::get_cid() {
	return cid_;
}

/*-------------------------------------------------------------------------------------------*/

FILE *make_socket(std::string host, std::string port) {
	FILE *socket_stream;

	/* Lookup server address information */
	struct addrinfo *results;
    struct addrinfo  hints = {
        .ai_flags    = 0,
        .ai_family   = AF_UNSPEC,   /* Return IPv4 and IPv6 choices */
        .ai_socktype = SOCK_STREAM, /* Use TCP */
    };
    int status;
    if ((status = getaddrinfo(host.c_str(), port.c_str(), &hints, &results)) != 0) {
    	fprintf(stderr, "getaddrinfo failed: %s\n", gai_strerror(status));
    }

    /* For each server entry, allocate socket and try to connect */
    int server_fd = -1;
    for (struct addrinfo *p = results; p != NULL && server_fd < 0; p = p->ai_next) {
		/* Allocate socket */
		if ((server_fd = socket(p->ai_family, p->ai_socktype, p->ai_protocol)) < 0) {
		    fprintf(stderr, "Unable to make socket: %s\n", strerror(errno));
		    continue;
		}

		/* Connect to host */
		if (connect(server_fd, p->ai_addr, p->ai_addrlen) < 0) {
		    fprintf(stderr, "Unable to connect to %s:%s: %s\n", host.c_str(), port.c_str(), strerror(errno));
		    close(server_fd);
		    server_fd = -1;
		    continue;
		}
	}
	freeaddrinfo(results);

    if (server_fd < 0) {
    	socket_stream = NULL;
    }

    socket_stream = fdopen(server_fd, "w+");
    if (socket_stream == NULL) {
        fprintf(stderr, "Unable to fdopen: %s\n", strerror(errno));
        close(server_fd);
    }

    return socket_stream;
}


/*-------------------------------------------------------------------------------------------*/
Thread::Thread() {}

Thread::Thread(void *client) {}

void Thread::start(thread_func func, void *arg) {
	int rc;
	PTHREAD_CHECK(pthread_create(&thread_, NULL, func, arg));
}

void Thread::join(void **result) {
	int rc;
	PTHREAD_CHECK(pthread_join(thread_, result));
}

void Thread::detach() {
	int rc;
	PTHREAD_CHECK(pthread_detach(thread_));
}