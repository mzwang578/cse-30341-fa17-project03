// SYSTEM INCLUDES

#include <gtest/gtest.h>

// C++ PROJECT INCLUDES

#include <ps_client/client.h>

void *generator_thread (void *arg) {
	for (int i = 0; i < 100000 ; ++i) {
		i = i+i;
	}
	return NULL;
}


TEST(QueueTest, PushPop) {
	Queue<Message> q;
	Message m;
	m.type_ = "MESSAGE";
	m.topic_ = "echo";
	m.sender_ = "pbui";
	m.nonce_ = 123;
	m.body_ = "152213123 001";

	q.push(m);

	Message p;
	p = q.pop();

	// ---------------------------
	EXPECT_EQ ("MESSAGE", p.type_);
	EXPECT_EQ ("echo", p.topic_);
	EXPECT_EQ ("pbui", p.sender_);
	EXPECT_EQ (unsigned(123), p.nonce_);
	EXPECT_EQ ("152213123 001", p.body_);
}

TEST(MessageTest, MessageValues) { 
	Message m;
	m.type_ = "MESSAGE";
	m.topic_ = "echo";
	m.sender_ = "pbui";
	m.nonce_ = 123;
	m.body_ = "152213123 001";

	// ---------------------------
	EXPECT_EQ ("MESSAGE", m.type_);
	EXPECT_EQ ("echo", m.topic_);
	EXPECT_EQ ("pbui", m.sender_);
	EXPECT_EQ (unsigned(123), m.nonce_);
	EXPECT_EQ ("152213123 001", m.body_);
}

TEST(ClientTest, ClientConstructor) { 
	Client c("localhost", "9711", "pbui");

	// ---------------------------
	EXPECT_EQ ("localhost", c.get_host());
	EXPECT_EQ ("9711", c.get_port());
	EXPECT_EQ ("pbui", c.get_cid());
}

TEST(ClientTest, ClientConnect) { 
	Client c("localhost", "9711", "pbui");

	// ---------------------------
	EXPECT_NO_THROW (c.connect_server());
}

TEST(ClientTest, ClientPublish) { 
	Client c("localhost", "9711", "pbui");

	// ---------------------------
	EXPECT_NO_THROW (c.publish("echo", "123456", 6));
}

TEST(ClientTest, ClientSubscribe) { 
	Client c("localhost", "9711", "pbui");
	EchoCallback e;

	// ---------------------------
	EXPECT_NO_THROW (c.subscribe("echo", &e));
}

TEST(ClientTest, ClientUnsubscribe) { 
	Client c("localhost", "9711", "pbui");

	// ---------------------------
	EXPECT_NO_THROW (c.unsubscribe("echo"));
}

TEST(ClientTest, ClientDisconnect) { 
	Client c("localhost", "9711", "pbui");

	// ---------------------------
	EXPECT_NO_THROW (c.disconnect());
	ASSERT_EQ (1, c.shutdown());
}

TEST(SocketTest, SocketMake) { 
	// ---------------------------
	EXPECT_NE ((FILE *)NULL, make_socket("localhost", "9711"));
}

TEST(ThreadTest, ThreadAll) {
	Thread t;
	size_t result;

	t.start(generator_thread, NULL);
	t.join((void **)&result);

	// ---------------------------
	ASSERT_EQ (0, result);
}

int main(int argc, char* argv[]) {
	::testing::InitGoogleTest(&argc, argv);
	return RUN_ALL_TESTS();
}
