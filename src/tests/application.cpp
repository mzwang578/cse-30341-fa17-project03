/**
 * Members: aluc, dluc, mwang6
 * application.cpp
 */

#include <poll.h>
#include <ps_client/client.h>

const int 	TIMEOUT = 1000;
std::string USERNAME;
std::string CHATROOM;

void *chatroom_reader(void *client) {
	system("clear");

	Client *c = (Client *)client;
	std::string username = USERNAME;
	std::string chatroom = CHATROOM;
	std::cout << "Welcome " << username << " to chatroom \"" << chatroom << "\"!" << std::endl;

	bool prompt = true;
	while (true) {
		/* Display prompt */
		if (prompt) {
			std::cout << "Message to chatroom... ";
			prompt = false;
		}

		/* Poll STDIN */
		struct 	pollfd pfd 	= {STDIN_FILENO, POLLIN|POLLPRI, 0};
		int 	result		= poll(&pfd, 1, TIMEOUT);

		/* Process event */
		if (result < 0) { 			/* Error */
			fprintf(stderr, "Unable to poll %s\n", strerror(errno));
		} else if (result == 0) {	/* Print messages from chatroom */

		} else {					/* Handle command */
			std::string input;
			std::cin >> input;
			if (input.compare("Quit") == 0) {
				break;
			} else {
				c->publish(chatroom, input, input.length());
			}
		}
	}

	c->disconnect();
	return NULL;
}


int main(int argc, char *argv[]) {
    // Parse command line arguments.
    if (argc != 4) {
    	fprintf(stderr, "Usage %s: host port client_id\n", argv[0]);
    	return EXIT_FAILURE;
    }

	system("clear");

	// Initialize variables with command line arguments.
    const char *host      = argv[1];
    const char *port      = argv[2];
    const char *client_id = argv[3];

	// Get client information.
	std::cout << "What is your username? ";
	std::string username;
	std::cin >> USERNAME;

	std::cout << "What chatroom would you like to enter? ";
	std::string chatroom;
	std::cin >> CHATROOM;

	// Start client.
	Client      client(host, port, client_id);
	Thread		reader;
	AppCallback a;

	// Start threads.
	reader.start(chatroom_reader, (void *)&client);

	// Detach thread.
	reader.detach();

	// Begin client
	client.subscribe(chatroom, &a);
	client.run();

	// client.disconnect();
	return 0;
}