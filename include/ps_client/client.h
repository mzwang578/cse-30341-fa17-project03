/**
 * Members: aluc, dluc, mwang6
 * client.h
 */

// client.h: PS Client Library -------------------------------------------------

#pragma once

#include <errno.h>
#include <netdb.h>
#include <netinet/in.h>
#include <pthread.h>
#include <semaphore.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <unistd.h>

#include "macros.h"

#include <iostream>
#include <map>
#include <queue>
#include <string>
#include <ctime>

#define 	log(M, ...)     fprintf(stdout, "[%5d]  INFO " M "\n", getpid(), ##__VA_ARGS__) //TODO: FIND BODY INSTEAD OF PID


/*-------------------------------------------------------------------------------------------*/

template <typename T>
class Queue {
private:
    std::queue<T>   data;
    pthread_mutex_t lock;
    pthread_cond_t  empty;

public:
    Queue() {
    	int rc;
    	Pthread_mutex_init(&lock, NULL);
    	Pthread_cond_init(&empty, NULL);
    }

    void push(const T &value) {
    	int rc;
    	Pthread_mutex_lock(&lock);
    	data.push(value);
    	Pthread_cond_signal(&empty);
	    Pthread_mutex_unlock(&lock);
	}
    
    T pop() {
    	int rc;

    	Pthread_mutex_lock(&lock);
    	while (data.empty()) {
    	    Pthread_cond_wait(&empty, &lock);
		}
		T value = data.front();
	    data.pop();

    	Pthread_cond_signal(&empty);
    	Pthread_mutex_unlock(&lock);
    	return value;
    }
};


/*-------------------------------------------------------------------------------------------*/

struct Message {
	std::string type_;        // Message type (MESSAGE, IDENTIFY, SUBSCRIBE, UNSUBSCRIBE, RETRIEVE, DISCONNECT)
	std::string topic_;       // Message topic
	std::string sender_;      // Message sender
	size_t      nonce_;       // Sender's nonce
	std::string body_;        // Message body
};

/*-------------------------------------------------------------------------------------------*/

class Callback {
public:
	Callback();
	virtual void run(Message &m) = 0;
};

/*-------------------------------------------------------------------------------------------*/

class EchoCallback : public Callback {
public:
    void run(Message &m);
};


class AppCallback : public Callback {
public:
    void run(Message &m);
};

/*-------------------------------------------------------------------------------------------*/

typedef void *(*thread_func)(void *);

class Thread {
public:
	Thread();
	Thread(void *);

	void start(thread_func func, void *arg);
	void join(void **result);
	void detach();

private:
	pthread_t thread_;
};

/*-------------------------------------------------------------------------------------------*/

typedef void (*callback_func)(Message m);

class Client {
public:
	Client(std::string host, std::string port, std::string cid);

	void connect_server();
	void publish(std::string topic, std::string message, size_t length);
	void subscribe(std::string topic, Callback *callback);
	void unsubscribe(std::string topic);
	void disconnect();  // write to bool shutdown
	void run();
	bool shutdown();

	static void *send_thread(void *c);
	static void *receive_thread(void *c);
	static void *process_thread(void *c);

	std::string get_host();
	std::string get_port();
	std::string get_cid();

	FILE *server_file_;
	std::string port_;
	std::string host_;

private:
	std::string cid_;
	size_t      nonce_;
	
	Queue<Message> incoming_;
	Queue<Message> outgoing_;
	
	pthread_mutex_t lock_ = PTHREAD_MUTEX_INITIALIZER;
	bool is_shutdown_ = false;
	
	std::map<std::string , Callback*> callback_map;
};

/*-------------------------------------------------------------------------------------------*/

FILE *make_socket(std::string h, std::string p);
