CSE.30341.FA17: Project 03
==========================

This is the documentation for [Project 03] of [CSE.30341.FA17].

Members
-------

1. Anthony Luc (aluc@nd.edu)
2. Donald Luc (dluc@nd.edu)
3. Michael Wang (mwang6@nd.edu)

Design
------

> 1. The client library needs to provide a `Client` class.
>
>   - What does the `Client` class need to keep track of?
>
>   - How will the `Client` connect to the server?
>
>   - How will it implement concurrent publishing, retrieval, and processing of
>     messages?
>
>   - How will it pass messages between different threads?
>
>   - What data members need to have access synchronized? What primitives will
>     you use to implement these?
>
>   - How will threads know when to block or to quit?
>
>   - How will the `Client` determine which `Callbacks` belong to which topics?

Response:
The `Client` class needs to keep track of a member `file descriptor` of the
server which utilizes the name of the `host`, the `port` number, and a `client
ID`. After calling getaddrinfo(), use connect() to the server file descriptor.
In the Client run() method threads should create detached threads for
concurrency. pthread_join() will help pass message from different threads.
Data members that need to be synchronized include incoming queue, outgoing
queue, and process queue. We will use a Monitor that has locks and condition
variables in a queue. Threads will quit based on the shutdown() method.
Threads will block if they do not have the lock. A map will pair a topic with
its callback.

> 2. The client library needs to provide a `Callback` class.
>
>   - What does the `Callback` class need to keep track of?
>
>   - How will applications use this `Callback` class?

Response:
The Callback class keeps track of a retrieve message queue. Applications will
scroll through the retrieve message queue and then call an appropriate
callback for the app.

> 3. The client library needs to provide a `Message` struct.
>
>   - What does the `Message` struct need to keep track of?
>
>   - What methods would be useful to have in processing `Messages`?

Response:
The Message struct keeps track of: type, topic, sender, nonce, and body. Some
useful methods to process Messages are based on the Message type. Examples are
subcribe(), unsubscribe(), retrieve(), and more.

> 4. The client library needs to provide a `Thread` class.
>
>   - What does the `Thread` class need to keep track of?
>
>   - What POSIX thread functions will need to utilize?


Response:
The Thread class keeps track of a single thread. It will use pthread_create(), 
pthread_join(), and pthread_detach().

> 5. You will need to perform testing on your client library.
>
>   - How will you test your client library?
>
>   - What will `echo_test` tell you?
>
>   - How will you use the Google Test framework?
>
>   - How will you incorporate testing at every stage of development?

Response:
On one terminal we will run the server and run our binary echo_test against it.
echo_test will tell us 10 messages have been published and that 10 messages
have been retrieved on the echo topic. We will use Google Test framework on
each of the functions in the library. We will write a unit test after each
function is implemented.

> 6. You will need to create an user application that takes advantage of the
>    pub/sub system.
>
>   - What distributed and parallel application do you plan on building?
>
>   - How will utilize the client library?
>
>   - What topics will you need?
>
>   - What callbacks will you need?
>
>   - What additional threads will you need?

Response:
We plan on building a real-time messaging application. The library will handle
messages by storing them into the library's respective queues of processing, 
sending, and retrieving. The topics will be the other user's name or a
chatroom. Callbacks would be... We will also need a thread to handle user input.

Demonstration
-------------

> Place a link to your demonstration slides on [Google Slides].

Errata
------

> Describe any known errors, bugs, or deviations from the requirements.
The user application does not either have a proper receive_thread() or process_thread() working. Many user apps can write to a specific chatroom in the server but the server cannot properly display the chatroom messages because of the receive_thread()/process_thread().

Extra Credit
------------

> Describe what extra credit (if any) that you implemented.

[Project 03]:       https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/project03.html
[CSE.30341.FA17]:   https://www3.nd.edu/~pbui/teaching/cse.30341.fa17/
[Google Slides]:    https://docs.google.com/a/nd.edu/presentation/d/1v7lvJkXnrow1I-f8Y72YjNLWCUw4aDFnlmjVKXM2CFU/edit?usp=sharing
